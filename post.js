import { startSession } from 'mongoose';
import{
    GET_POSTS,
    POST_ERROR 
}from '../actions/types';
const initialState = {
    posts: [],
    post: null,
    loading: true,
    error:{}
}

export default function (state = initialState,action) {
    const{ type,payload } = action;
    switch(type) {
        case GET_POSTS:
            return{
                ...state,
                posts:payload,
                loading: false
            }
            case GET_ERROR:
            return{
                ...state,
                Error:payload,
                loading: false
    };
    default :
    return state;
}
}